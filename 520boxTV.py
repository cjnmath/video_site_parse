import requests
import urllib
from lxml.html import fromstring
import re


class boxTV:
    def __init__(self, main_url):
        self.main_url = main_url
        self.host = urllib.parse.urlparse(main_url).netloc
        self.session = requests.Session()
        self.main_root = self.get_main_root()
        self.name = self.get_main_episode_name()

    def get_main_root(self):
        headers = {"User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:56.0) Gecko/20100101 Firefox/56.0"}
        response = self.session.get(self.main_url, headers=headers)
        response.encoding = 'utf-8'
        root = fromstring(response.text)
        return root

    def get_main_episode_name(self):
        raw_title = self.main_root.xpath('//div[@class="detail-ms-left"]//img/@title')
        title = raw_title[0] if raw_title else None
        return title

    def get_episode_urls(self):
        result = {}
        raw_episode_urls = self.main_root.xpath('//div[@class="rb_title"]//p[contains(text(), "在线播放")]/../../following-sibling::div[1]//li/a/@href')

        episode_urls = [urllib.parse.urljoin('http://'+self.host, i) for i in raw_episode_urls]
        for num, url in enumerate(episode_urls):
            result[str(num)] = url
        return result

    def get_episode_video_url(self, episode_url):
        headers = {
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:56.0) Gecko/20100101 Firefox/56.0",
            'Host': self.host}
        response = self.session.get(episode_url, headers=headers)
        root = fromstring(response.text)
        raw_video_list = root.xpath('//iframe/@src')
        raw_video_url = raw_video_list[0] if raw_video_list else None
        print(raw_video_url)

        def get_special_embeded(url):
            response = self.session.get(url, headers=headers)
            match = re.search('(?<=var main = \")(.*)\"', response.text)
            if match:
                result = urllib.parse.urljoin(url, match.group(1))
            else:
                result = None
            return result

        if raw_video_url:
            # video_url = re.search('url\=(.*\.mp4|m3u8)', raw_video_url).group(1)
            video_url_match = re.search('(?<=url\=)(.+)', raw_video_url)
            if video_url_match:
                video_url = video_url_match.group()
            else:
                video_url = get_special_embeded(raw_video_url)
            return video_url
        else:
            return None

    def get_all_episode_video_urls(self):
        result = {}
        for num, url in self.get_episode_urls().items():
            # print(num, url)
            result[int(num)+1] = self.get_episode_video_url(url)
        return result
