import requests
import urllib
from lxml.html import fromstring
import re
import concurrent.futures

MAIN_HEADERS = {
            "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/72.0.3626.119 Chrome/72.0.3626.119 Safari/537.36",
            # "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
            # 'accept-encoding': 'gzip, deflate, br',
            # 'accept-language': 'en-US,en;q=0.9',
            # 'cache-control': 'max-age=0',
            # 'upgrade-insecure-requests': '1'
        }


class hangjuTV:
    def __init__(self, main_url):
        self.main_url = main_url
        self.host = 'https://' + urllib.parse.urlparse(main_url).netloc
        self.headers = MAIN_HEADERS
        self.session = requests.Session()
        self.main_response = self.session.get(main_url, headers=self.headers)
        self.main_response.encoding = 'utf-8'
        self.main_root = fromstring(self.main_response.text)
        self.name = self.get_main_episode_name()
        self.m3u8_urls_and_durations = self.get_all_episode_m3u8_urls_and_durations()

    def get_main_episode_name(self):
        raw_title = self.main_root.xpath('//div[contains(@class, "albumDetailMain")]//h1[@class="title"]/text()')
        title = raw_title[0] if raw_title else None
        return title

    def get_episode_urls(self):
        result = {}
        root = self.main_root
        if 'dianying' in self.main_url:
            raw_results_list = root.xpath('//a[@class="hjtvui-btn hjtvui-btn-radius"]/@href')
        else:
            raw_results_list = root.xpath('//div[contains(@class, "detailList-select")]//li/a/@href')
        results_list = [urllib.parse.urljoin(self.host, i) for i in raw_results_list]
        for i, r in enumerate(results_list):
            result["第{}集".format(str(i+1))] = r
        return result

    def get_episode_duration(self, m3u8_url, current_depth=0):
        _depth = current_depth
        if _depth > 3:
            return None
        try:
            response = requests.get(m3u8_url)
            if response.status_code != 200:
                return None
        except Exception:
            return None
        a = re.findall('(?<=\#EXTINF\:)\d+\.\d+', response.text)
        if a:
            b = [float(i) for i in a]
            duration = int(sum(b))
            return duration
        else:
            r_new_m3u8_url = None
            lines = response.text.splitlines()
            for line in lines:
                if line.endswith('.m3u8'):
                    r_new_m3u8_url = line
            if r_new_m3u8_url:
                new_m3u8_url = urllib.parse.urljoin(m3u8_url, r_new_m3u8_url)
                _depth += 1
                return self.get_episode_duration(new_m3u8_url, current_depth=_depth)
            else:
                return None

    def get_episode_m3u8_urls(self, episode_url):

        def get_source(response):
            source = re.findall("(?<=_switchSource\(\')(.*?)(?=\'\,)", response.text)
            return source

        def switch_to_http(url):
            if re.search('https', url):
                url = re.sub('https', 'http', url)
            return url

        headers = {
            "Referer": episode_url,
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:56.0) Gecko/20100101 Firefox/56.0"
        }
        episode_response = self.session.get(episode_url, headers=headers)
        while episode_response.status_code != 200:
            episode_response = self.session.get(episode_url, headers=headers)

        # get sources
        sources = get_source(episode_response)

        # get m3u8_urls from sources
        m3u8_urls = [None for _ in range(len(sources))]
        for i in range(len(sources)):
            src_url = 'http://ww4.hanjutv.com/index.php?path=' + sources[i]

            k = 0
            source_response = self.session.get(src_url, headers=headers)
            while source_response.status_code != 200 and k < 3:
                source_response = self.session.get(src_url, headers=headers)
                k += 1

            if source_response.status_code == 200:
                r_m3u8_url = re.search('http.*\.m3u8', source_response.text)
                if r_m3u8_url:
                    m3u8_url = r_m3u8_url.group()
                    m3u8_url = switch_to_http(m3u8_url)
                    m3u8_urls[i] = m3u8_url
        return m3u8_urls

    def get_episode_m3u8_url_and_duration(self, episode_url):
        # get m3u8 urlparse
        m3u8_urls = self.get_episode_m3u8_urls(episode_url)
        for url in m3u8_urls:
            if url:
                duration = self.get_episode_duration(url)
                if duration:
                    a = []
                    a.append(url)
                    return a, duration
            if url == m3u8_urls[-1]:
                return a, 88888888888

    def get_all_episode_m3u8_urls_and_durations(self):
        result = {}
        target_urls = []

        def take_record(num, url):
            print(url)
            result[num] = self.get_episode_m3u8_url_and_duration(url)
        for num, episode_url in self.get_episode_urls().items():
            target_urls.append((num, episode_url))

        with concurrent.futures.ThreadPoolExecutor(max_workers=500000) as executor:
            for item in target_urls:
                executor.submit(take_record, *item)

        return result
