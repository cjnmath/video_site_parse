from utiles import get_response
from lxml.html import fromstring
import re
import json
import concurrent.futures


def extract_playlist_url(page_url):
    result = []
    response = get_response(page_url)
    response.encoding = 'utf-8'
    root = fromstring(response.text)
    name = ''.join(root.xpath('//div[@class="player_header player_header_simple"]//a/text()')).strip()
    t = re.search('var COVER_INFO = (\{.*\})', response.text)

    def switch_id(new_id):
        l = page_url.split('/')
        for i in l:
            if 'html' in i:
                re.sub('.*\.html', new_id+'.html', i)
        return '/'.join(l)
    if t:
        dl = json.loads(t.group(1))
        for i in  range(len(dl['video_ids'])):
            d = {}
            d['title'] = name + " - 第{}集".format(str(i+1))
            d['url'] = switch_id(dl['video_ids'][i])
            d['duration'] = None
            result.append(d)
    return result


def get_duration(page_url):
    response = get_response(page_url)
    duration = re.search('duration\":\"(\d+)\"', response.text)
    if duration:
        return duration.group(1)


def assemble_data(page_url):
    result = extract_playlist_url(page_url)

    def wrapper(i):
        duartion = get_duration(i['url'])
        max_try = 2
        while not duartion and max_try>0:
            duartion = get_duration(i['url'])
            print('retry', max_try, i['url'])
            max_try -= 1
        i['duration'] = duartion
        print('get youku duration for:', i['url'])

    with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:
        for i in result:
            executor.submit(wrapper, i)
    return result
