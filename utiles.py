import requests
from lxml.html import fromstring
import urllib
import re


HEADERS = {
    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/73.0.3683.86 Chrome/73.0.3683.86 Safari/537.36"
}

MOBILE_HEADERS = {
    "User-Agnet": "Mozilla/5.0 (Linux; Android 7.0; PLUS Build/NRD90M) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.98 Mobile Safari/537.36"
}


def get_response(url, params=None, headers=HEADERS, proxies=None):
    response = requests.get(url, params=params, headers=headers, proxies=proxies)
    return response


def get_root(url, params=None, headers=HEADERS, encoding='utf-8', proxies=None):
    response = get_response(url, params=params, headers=headers, proxies=proxies)
    response.encoding = encoding
    root = fromstring(response.text)
    return root


def get_m3u8_duration(m3u8_url, current_depth=0):
    _depth = current_depth
    if _depth > 5:
        return None
    try:
        response = requests.get(m3u8_url)
        if response.status_code != 200:
            return None
    except Exception:
        m3u8_url = re.sub('^http', 'https', m3u8_url)
        response = requests.get(m3u8_url)
        if response.status_code != 200:
            return None
    a = re.findall('(?<=\#EXTINF\:)\d+\.\d+', response.text)
    if a:
        b = [float(i) for i in a]
        duration = int(sum(b))
        return duration
    else:
        r_new_m3u8_url = None
        lines = response.text.splitlines()
        for line in lines:
            if line.endswith('.m3u8'):
                r_new_m3u8_url = line
        if r_new_m3u8_url:
            new_m3u8_url = urllib.parse.urljoin(m3u8_url, r_new_m3u8_url)
            _depth += 1
            return get_m3u8_duration(new_m3u8_url, current_depth=_depth)
        else:
            return None
