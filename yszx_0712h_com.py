from utiles import get_response
from utiles import get_root
from utiles import HEADERS
import re
import urllib
from utiles import get_m3u8_duration
import concurrent.futures


def extract_all_page_url(main_url):
    result = {}
    root = get_root(main_url)
    title = root.xpath('//h3/text()')[0]
    playlist_tags = root.xpath('//div[@id="playlist"]//div[@class="panel clearfix"]')
    for playlist_tag in playlist_tags:
        playlist_num_r = playlist_tag.xpath('.//a[contains(text(), "播放线路")]/@href')[0]
        playlist_num = re.search('\d+', playlist_num_r).group()
        result['线路{}'.format(playlist_num)] = {}

        episode_names = playlist_tag.xpath('.//li/a/text()')
        episode_page_urls  = [urllib.parse.urljoin(main_url, url) for url in playlist_tag.xpath('.//li/a/@href')]
        result['线路{}'.format(playlist_num)] = []
        for i in range(len(episode_names)):
            s = (title + ' - ' + episode_names[i], episode_page_urls[i])
            result['线路{}'.format(playlist_num)].append(s)

    return result


def get_play_url_and_duration(page_url):
    root = get_root(page_url)

    play_url_mask = None
    for script in root.xpath('//script/text()'):
        search_result = re.search('var now=\"(.*?)\"', script)
        if search_result:
            play_url_mask = search_result.group(1)
            break
    if play_url_mask.endswith('.m3u8'):
        play_url_mask = play_url_mask.replace('https', 'http')
        duration = get_m3u8_duration(play_url_mask)
        return play_url_mask, duration
    if not play_url_mask:
        print("no play_url_mask")
        return None

    mask_response = get_response(play_url_mask)
    signed_m3u8_url = None
    search_signed_m3u8_url = re.search('var main = \"(.+?)\"', mask_response.text)

    if search_signed_m3u8_url:
        signed_m3u8_url = search_signed_m3u8_url.group(1)
    # redirecturl = None
    # search_redirecturl = re.search('var redirecturl = \"(.+?)\"', mask_response.text)
    # if search_redirecturl:
    #     redirecturl = search_redirecturl.group(1)
    # if not signed_m3u8_url or not redirecturl:
        # print('no signed_m3u8_url or redirecturl')
    if not signed_m3u8_url:
        print('no signed_m3u8_url')
        return None

    mask_m3u8_url = urllib.parse.urljoin(play_url_mask, signed_m3u8_url)
    headers = HEADERS.copy()
    headers['Referer'] = play_url_mask
    mask_m3u8_url = mask_m3u8_url.replace('https', 'http')
    m3u8_response = get_response(mask_m3u8_url, headers=headers)

    search_m3u8_url = re.search('/?.+\.m3u8', m3u8_response.text)
    m3u8_url = None
    if search_m3u8_url:
        m3u8_url = search_m3u8_url.group()
        # m3u8_url = urllib.parse.urljoin(play_url_mask, m3u8_url)
        m3u8_url = urllib.parse.urljoin(mask_m3u8_url, m3u8_url)

    user_agent = "Mozilla/5.0 (Linux; Android 7.0; PLUS Build/NRD90M) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.98 Mobile Safari/537.36"
    header_slur = '@@referer=' + headers['Referer'] + '@user-agent=' + user_agent + "@@"
    link =  m3u8_url+header_slur if m3u8_url else mask_m3u8_url+header_slur
    duration = get_m3u8_duration(m3u8_url) if m3u8_url else get_m3u8_duration(mask_m3u8_url)
    return link, duration


def assemble_data(all_page_url):
    result = {}
    for k in all_page_url.keys():
        result[k] = ['a' for i in range(len(all_page_url[k]))]

    def wrapper(playlist_name, title, page_url):
        r = {}
        m3u8_url, duration = get_play_url_and_duration(page_url)
        r['title'] = title
        r['m3u8_url'] = m3u8_url
        r['duration'] = duration
        print('get', playlist_name, title, page_url)
        index = all_page_url[playlist_name].index((title, page_url))
        result[playlist_name][index] = r

    with concurrent.futures.ThreadPoolExecutor(max_workers=200) as executor:
        for k, v in all_page_url.items():
            for a, b in v:
                p = [k, a, b]
                executor.submit(wrapper, *p)
    return result
