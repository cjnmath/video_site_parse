from utiles import get_response
import requests
import json
import base64
import time
from reporter import QimaiReporter as qr
from datetime import datetime as dt
from datetime import date, timedelta
import mysql.connector


# SESSION =


def get_search_terms():
    url = "http://121.40.94.43:8001/conf/get?confGroup=crawlerConf&confCode=searchKeywords"
    response = get_response(url)
    d = json.loads(response.text)
    if d.get('data'):
        return json.loads(d['data'])
    else:
        qr.send_msg('[!!!] {} 获取搜索关键词失败，搜索任务终止!'.format(dt.now()))
        return None


def encrypt(a):
    n = '00000008d78d46a'
    # n = '99602'
    s, n = list(a), list(n)
    sl, nl = len(s), len(n)
    print('length', sl)
    for i in range(0, sl):
        s[i] = chr(ord(s[i]) ^ ord(n[(i+10) % nl]))
    return "".join(s)


def get_analysis(params, field='/rank/release'):
    diff_time = str(int(time.time() * 1000 - 1515125653845))
# def get_analysis(params, diff_time):
#     diff_time = str(diff_time)
    params_r = "".join(sorted([str(v) for v in params.values()]))
    params_r = base64.b64encode(bytes(params_r, encoding="ascii"))
    # field = '/rank/release'
    # field = '/account/ucenterSignin'
    s = "@#".join([params_r.decode(), field, diff_time, '1'])
    # s = '@#' + s
    print(s)
    s = base64.b64encode(bytes(encrypt(s), encoding="ascii"))
    analysis = s.decode()
    return analysis


def get_field(url):
    return url.replace('https://api.qimai.cn', '')


def get_login_session():
    HEADERS = {
        'Accept': 'application/json, text/plain, */*',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        'Origin': 'https://www.qimai.cn',
        'Referer': 'https://www.qimai.cn/account/signin/r/%2F',
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/74.0.3729.169 Chrome/74.0.3729.169 Safari/537.36'
    }

    data = {
        'username': '13751741153',
        'password': '137qimai0927'
    }
    # data = {
    #     'username': '19875675437',
    #     'password': '198qimai0927'
    # }

    session = requests.Session()
    # url = 'https://api.qimai.cn/account/signinForm?analysis=cBMfUVNTX00KQxcXXVEPWV52X0JdcBtXAA9SAAVSBwEFAnATAQ%3D%3D'
    url = 'https://api.qimai.cn/account/signinForm'
    # url = 'https://api.qimai.cn/account/ucenterSignin'
    # field = '/account/ucenterSignin'
    field = get_field(url)
    params = data
    params['analysis'] = get_analysis({}, field=field)
    response = session.post(url, headers=HEADERS, params=params, data=data)
    response.encoding = 'utf-8'
    d = json.loads(response.text)
    # if d['msg'] != "登录成功":
    #     qr.send_msg('[!!!] {} 登录失败！'.format(dt.now()), at_me=True)
    #     return None
    return d


def get_single_page_by_date(session, date, page_num=1):
    params = {
        'genre': '36',
        'date': date,
        'page': str(page_num)
    }
    analysis = get_analysis(params)
    params['analysis'] = analysis

    headers = {
        'Accept': 'application/json, text/plain, */*',
        'Origin': 'https://www.qimai.cn',
        'Referer': 'https://www.qimai.cn/rank/release/genre/36/date/{}'.format(date),
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/73.0.3683.75 Chrome/73.0.3683.75 Safari/537.36'}
    url = "https://api.qimai.cn/rank/release"
    response = session.get(url, headers=headers, params=params)
    print(response.url)
    d = json.loads(response.text)
    # if d['code'] == 10000:
    #     return d
    # else:
    #     return None
    return d


def get_all_page_by_date(session, date):
    result = []
    first_page_d = get_single_page_by_date(session, date)
    if not first_page_d:
        pass # TODO:
        return
    result.extend(first_page_d['rankInfo'])
    maxPage = first_page_d['maxPage']
    if maxPage > 1:
        for i in range(maxPage):
            k = i+1
            if k == 1:
                continue
            result.extend(get_single_page_by_date(session, date, page_num=k)['rankInfo'])
    return result


def is_search_app(search_terms, app_info_item):
    app_name = app_info_item['appInfo']['appName']
    for term in search_terms:
        if term in app_name:
            return True
    return False


def extract_target_apps(search_terms, pages):
    result = []
    # result_name = '\n'
    for app_info_item in pages:
        if is_search_app(search_terms, app_info_item):
            result.append(app_info_item)
            # result_name += app_info_item['appInfo']['appName'] + " (发布日期: {})".format(app_info_item['releaseTime']) + '\n'

    # date = dt.now()
    # num = len(result)
    # qr.send_msg('[---] {} 获取{}条合适的信息。它们分别是:{}'.format(date, num, result_name))
    return result


def load_app_ids():
    mysql_server_info = {
        'user': 'chenjiangnan',
        'password': 'Z170qBnEn16M7vCq',
        'host': 'rdsrnqrbvinrzq2.mysql.rds.aliyuncs.com',
        # 'host': '127.0.0.1',
        'port': '3306',
        'database': 'yuntu_stats'
    }
    connection = mysql.connector.connect(**mysql_server_info)
    cursor = connection.cursor()
    query = """
            SELECT * FROM keyword_up_app
            """
    cursor.execute(query)
    result = []
    for row in cursor:
        result.append(row[1])
    return result


def write_in_db(pages):
    """
        CREATE TABLE `keyword_up_app` (
              `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
              `appId` varchar(32) NOT NULL,
              `country` varchar(10) DEFAULT NULL,
              `appName` varchar(32) DEFAULT NULL,
              `icon` varchar(256) DEFAULT NULL,
              `price` varchar(8) DEFAULT NULL,
              `publisher` varchar(128) DEFAULT NULL,
              `genre` varchar(32) DEFAULT NULL,
              `releaseTime` varchar(32) DEFAULT NULL,
              `createTime` datetime DEFAULT CURRENT_TIMESTAMP,
              PRIMARY KEY (`id`),
              UNIQUE KEY `appId` (`appId`)
            ) ENGINE=InnoDB AUTO_INCREMENT=73790 DEFAULT CHARSET=utf8mb4
    """
    mysql_server_info = {
        'user': 'chenjiangnan',
        'password': 'Z170qBnEn16M7vCq',
        'host': 'rdsrnqrbvinrzq2.mysql.rds.aliyuncs.com',
        # 'host': '127.0.0.1',
        'port': '3306',
        'database': 'yuntu_stats'
    }
    old_app_ids = load_app_ids()
    try:
        connection = mysql.connector.connect(**mysql_server_info)
        cursor = connection.cursor()

        insert_count = 0
        for app_info_item in pages:
            add = ("""
                    INSERT INTO keyword_up_app (appId, country, appName, icon, price, publisher, genre, releaseTime, createTime)
                    VALUES
                    (%(appId)s, %(country)s, %(appName)s, %(icon)s, %(price)s, %(publisher)s, %(genre)s, %(releaseTime)s, %(createTime)s)
                    """)
            data = {
                    'appId': app_info_item['appInfo']['appId'],
                    'country': app_info_item['appInfo']['country'],
                    'appName': app_info_item['appInfo']['appName'],
                    'icon': app_info_item['appInfo']['icon'],
                    'price': app_info_item['price'],
                    'publisher': app_info_item['appInfo']['publisher'],
                    'genre': app_info_item['genre'],
                    'releaseTime': app_info_item['releaseTime'],
                    'createTime': dt.now()}
            if data['appId'] in old_app_ids:
                continue
            old_app_ids.append(data['appId'])
            insert_count += 1
            cursor.execute(add, data)
            print(data['appId'])
        connection.commit()
        cursor.close()
        connection.close()
        qr.send_msg("[---] 成功插入{}条信息。".format(str(insert_count)))

    except Exception as e:
        qr.send_msg("[---]数据库链接错误", at_me=True)
        print(e)


def main():
    session = get_login_session()
    yesterday = date.today() - timedelta(1)
    r_date = yesterday.strftime('%Y-%m-%d')
    pages = get_all_page_by_date(session, r_date)
    if pages:
        write_in_db(pages)
        print('get pages')
    search_terms = get_search_terms()
    if search_terms:
        raw_feeds = extract_target_apps(search_terms, pages)
        feed_num = len(raw_feeds)
        # if feed_num > 0:
        msg = '[---] 本次监测到{}个重点应用。'.format(str(feed_num))
        qr.send_msg(msg)

        count = 1
        for raw_feed in raw_feeds:
            msg = '\n'
            title = str(count) + ". 名称：　" + raw_feed['appInfo']['appName'] + '\n'
            msg += title
            base_url = "https://www.qimai.cn/app/rank/appid/{}"
            link = "链接：　" +base_url.format(raw_feed['appInfo']['appId']) + '\n'
            msg += link
            count += 1
            qr.send_msg(msg)


    # print(r_date)
    return pages

# if __name__ == '__main__':
#     d = main()
#     pass
