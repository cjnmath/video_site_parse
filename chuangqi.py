from utiles import get_response
from datetime import datetime as dt
import json
from reporter import ChuangqiReporter as cr
import mysql.connector
from datetime import timedelta
from datetime import date

HEADERS = {
    'Accept': 'application/json, text/plain, */*',
    'User-Agent': 'Mozilla/5.0 (Linux; Android 4.4.2; Nexus 4 Build/KOT49H) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.114 Mobile Safari/537.36',
    'Origin': 'https://m.cqaso.com',
    'Referer': 'https://m.cqaso.com/toplist/newApp'
}


def get_all_contents(date, country='CN'):
    result = []
    params = {
        'dateStart': date,
        'dateEnd': date,
        'limit': 500,
        'offset': 0,
        'country': country,
        'genre': '36'
    }
    api_url = 'https://backend.cqaso.com/app/takeUpNew'
    while True:
        response = get_response(api_url, params=params, headers=HEADERS)
        print(response.url)
        d = response.json()
        total = d['total']
        result.extend(d['contents'])
        params['offset'] += params['limit']
        if params['offset'] > total:
            print(total)
            break
    for item in result:
        item['country'] = country
    return result


def get_search_terms():
    url = "http://121.40.94.43:8001/conf/get?confGroup=crawlerConf&confCode=searchKeywords"
    response = get_response(url)
    d = json.loads(response.text)
    if d.get('data'):
        return json.loads(d['data'])
    else:
        cr.send_msg('[!!!] {} 获取搜索关键词失败，搜索任务终止!'.format(dt.now()))
        return None


def is_search_app(search_terms, app_info_item):
    app_name = app_info_item['name']
    for term in search_terms:
        if term in app_name:
            return True
    return False


def extract_target_apps(search_terms, contents):
    result = []
    for app_info_item in contents:
        if is_search_app(search_terms, app_info_item):
            result.append(app_info_item)
    return result


def load_app_ids():
    mysql_server_info = {
        'user': 'chenjiangnan',
        'password': 'Z170qBnEn16M7vCq',
        'host': 'rdsrnqrbvinrzq2.mysql.rds.aliyuncs.com',
        # 'host': '127.0.0.1',
        'port': '3306',
        'database': 'yuntu_stats'
    }
    connection = mysql.connector.connect(**mysql_server_info)
    cursor = connection.cursor()
    query = """
            SELECT * FROM keyword_up_app
            """
    cursor.execute(query)
    result = []
    for row in cursor:
        result.append(row[1])
    return result


def write_in_db(contents):
    """
        CREATE TABLE `keyword_up_app` (
              `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
              `appId` varchar(32) NOT NULL,
              `country` varchar(10) DEFAULT NULL,
              `appName` varchar(32) DEFAULT NULL,
              `icon` varchar(256) DEFAULT NULL,
              `price` varchar(8) DEFAULT NULL,
              `publisher` varchar(128) DEFAULT NULL,
              `genre` varchar(32) DEFAULT NULL,
              `releaseTime` varchar(32) DEFAULT NULL,
              `createTime` datetime DEFAULT CURRENT_TIMESTAMP,
              PRIMARY KEY (`id`),
              UNIQUE KEY `appId` (`appId`)
            ) ENGINE=InnoDB AUTO_INCREMENT=73790 DEFAULT CHARSET=utf8mb4
    """
    mysql_server_info = {
        'user': 'chenjiangnan',
        'password': 'Z170qBnEn16M7vCq',
        'host': 'rdsrnqrbvinrzq2.mysql.rds.aliyuncs.com',
        # 'host': '127.0.0.1',
        'port': '3306',
        'database': 'yuntu_stats'
    }
    old_app_ids = load_app_ids()
    try:
        connection = mysql.connector.connect(**mysql_server_info)
        cursor = connection.cursor()

        insert_count = 0
        for app_info_item in contents:
            add = ("""
                    INSERT INTO keyword_up_app (appId, country, appName, icon, price, publisher, genre, releaseTime, createTime)
                    VALUES
                    (%(appId)s, %(country)s, %(appName)s, %(icon)s, %(price)s, %(publisher)s, %(genre)s, %(releaseTime)s, %(createTime)s)
                    """)
            data = {
                    'appId': 'chuangqi_' + str(app_info_item['appId']),
                    'country': app_info_item['country'],
                    'appName': app_info_item['name'],
                    'icon': app_info_item.get('artworkUrl60', 'no icon'),
                    'price': str(app_info_item['price']),
                    'publisher': 'no_publisher',
                    'genre': app_info_item['primaryGenreId'],
                    'releaseTime': app_info_item['time'],
                    'createTime': dt.now()}
            if data['appId'] in old_app_ids:
                continue
            old_app_ids.append(data['appId'])
            insert_count += 1
            cursor.execute(add, data)
            print(data['appId'])
        connection.commit()
        cursor.close()
        connection.close()
        cr.send_msg("[---] 成功插入{}条信息。".format(str(insert_count)))

    except Exception as e:
        cr.send_msg("[---] 数据库链接错误", at_me=True)
        print(e)


def main():
    yesterday = date.today() - timedelta(1)
    r_date = yesterday.strftime('%Y%m%d')

    raw_feeds = []
    raw_feed_ids = []

    country_list = ['CN', 'US']
    for country in country_list:
        contents = get_all_contents(r_date, country=country)
        if contents:
            write_in_db(contents)
            print('wrote {} datas in db'.format(len(contents)))

        search_terms = get_search_terms()
        if search_terms:
            target_apps = extract_target_apps(search_terms, contents)
            for target_app in target_apps:
                if target_app['appId'] not in raw_feed_ids:
                    raw_feeds.append(target_app)
                    raw_feed_ids.append(target_app['appId'])

    feed_num = len(raw_feeds)
    msg = '[---] 本次监测到{}个重点应用。(数据日期:{})'.format(str(feed_num), r_date)
    print(raw_feeds)
    cr.send_msg(msg)

    count = 1
    for raw_feed in raw_feeds:
        msg = ''
        title = str(count) + ". 名称：　" + raw_feed['name'] + '\n'
        msg += title
        base_url = "https://m.cqaso.com/app/{}/rank/{}"
        link = "  链接：　" + base_url.format(raw_feed['appId'], raw_feed['country']) + '\n'
        msg += link
        count += 1
        cr.send_msg(msg)


if __name__ == '__main__':
    main()
