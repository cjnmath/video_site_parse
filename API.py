from flask import Flask, jsonify
from flask_restful import Resource, Api, reqparse
from hangjuTV_asyn_threading import hangjuTV
from krcom_cn import multiple_episodes
from krcom_cn import single_episode
import bilibili_extractor
import yanshiyingyin
import aiqiyi_extractor
import yszx_0712h_com
import youku_extractor
import qq_video_extractor
# from qimai import

app = Flask(__name__)
api = Api(app)


class get_yangshiyingyin_info(Resource):
    def get(self):
        result = {}
        args = parser.parse_args()
        page_num = args['p']
        category = args['category']
        result['msg'] = 'success'
        if category == 'aikan':
            data = yanshiyingyin.get_aikan_info_list(page_num)
        elif category == 'xiaoyan':
            data = yanshiyingyin.get_xiaoyanshiping_info_list(page_num)
        elif category == 'xinwen':
            data = yanshiyingyin.get_xinwen_info_list(page_num)
        else:
            result['msg'] = 'No Such Category'
            data = []
        result['data'] = data
        return jsonify(result)


class extract_page_urls(Resource):
    def get(self):
        """
            {'data': [{'duration': 1500,
                       'title': '1. 第一集',
                       'url': 'https://www.bilibili.com/video/av28718980?p=1'},
                      {'duration': 1500,
                       'title': '2. 第二集',
                       'url': 'https://www.bilibili.com/video/av28718980?p=2'},
                      {'duration': 1500,
                       'title': '3. 第三集',
                       'url': 'https://www.bilibili.com/video/av28718980?p=3'},
                      {'duration': 1500,
                       'title': '4. 第四集',
                       'url': 'https://www.bilibili.com/video/av28718980?p=4'},
                      {'duration': 1500,
                       'title': '5. 第五集',
                       'url': 'https://www.bilibili.com/video/av28718980?p=5'},
                      {'duration': 1500,
                       'title': '6. 第六集',
                       'url': 'https://www.bilibili.com/video/av28718980?p=6'},
                      {'duration': 31,
                       'title': '30秒宣传片',
                       'url': 'https://www.bilibili.com/video/av28718980?p=7'},
                      {'duration': 56,
                       'title': '55秒宣传片',
                       'url': 'https://www.bilibili.com/video/av28718980?p=8'}],
                     'msg': 'success!
                     'code': 0}
        """
        result = {}
        args = parser.parse_args()
        target_url = args['target_url']
        if 'iqiyi.com' in target_url:
            result['data'] = aiqiyi_extractor.extract_all_page_urls(target_url)
            result['msg'] = 'success!'
            result['code'] = 0

        elif 'bilibili.com' in target_url:
            result['msg'] = 'success!'
            target_url = target_url.split('?').pop(0)
            if 'video/av' in target_url:
                result['data'] = bilibili_extractor.extrat_pages_from_av_links(target_url)['parse_result']
            elif 'bangumi' in target_url:
                if 'play' in target_url:
                    result['data'] = bilibili_extractor.extract_episodes_from_bangumi_play_links(target_url)['parse_result']
                elif 'media' in target_url:
                    result['data'] = bilibili_extractor.extract_episodes_from_bangumi_media_links(target_url)['parse_result']
                else:
                    result['data'] = []
            else:
                result['data'] = []
            result['code'] = 0

        elif 'krcom.cn' in target_url:
            result['msg'] = 'success'
            try:
                sub_results = multiple_episodes.parse(target_url)
            except:
                sub_results = {}
                sub_results[1] = single_episode.parse(target_url)
            l = []
            for i in range(len(sub_results)):
                l.append({
                            'title': str(i) + '. ' + sub_results[i+1][0],
                            'url': sub_results[i+1][1],
                            'duration': sub_results[i+1][2]})
            result['data'] = l
            result['code'] = 0

        elif 'youku.com' in target_url:
            result['msg'] = 'success'
            result['data'] = youku_extractor.assemble_data(target_url)
            result['code'] = 0

        elif 'v.qq.com' in target_url:
            result['msg'] = 'success'
            result['data'] = qq_video_extractor.assemble_data(target_url)
            result['code'] = 0

        else:
            result['msg'] = 'errors, not supported url'
            result['data'] = []
            result['code'] = 3
        return jsonify(result)


class extract_m3u8_urls(Resource):
    def get(self):
        """
            result := {
                    msg: success,
                    data: {<线路名01>:[
                                        {
                                        'title': '第01集'，
                                        'm3u8_url': 'http://saklkalalkalks.m3u8',
                                        duration: 1234},
                                        {...},
                                        {...}
                            ],
                            <线路名02>：[
                                        {...},
                                        {...},
                            ]

                    }
            }
        """
        result = {}
        args = parser.parse_args()
        target_url = args['target_url']
        print(target_url)

        if 'hanjutv.com' in target_url:
            target = hangjuTV(args['target_url'])
            result['msg'] = 'success'
            result['data'] = {}
            sub_result = []
            for i in range(len(target.m3u8_urls_and_durations)):
                    k = '第{}集'.format(str(i+1))
                    v = target.m3u8_urls_and_durations[k]
                    sub_result.append({"title": target.name + ': ' + k, "m3u8_url": v[0], "duration": v[1]})
            result['data']['线路01'] = sub_result

        elif 'yszxwang.com' in target_url:
            result['msg'] = 'success'
            all_page_url = yszx_0712h_com.extract_all_page_url(target_url)
            result['data'] = yszx_0712h_com.assemble_data(all_page_url)
            result['code'] = 0

        else:
            result['msg'] = 'errors! not supported url!'
            result['data'] = {}
            result['code'] = 3
        return jsonify(result)


parser = reqparse.RequestParser()
parser.add_argument('target_url')
parser.add_argument('p')
parser.add_argument('category')
parser.add_argument('date')
api.add_resource(get_yangshiyingyin_info, '/get_yangshiyingyin_parse')
api.add_resource(extract_page_urls, '/extract_page_urls')
api.add_resource(extract_m3u8_urls, '/extract_m3u8_urls')
# api.add_resource(get_qimai_info, '/get_qimai_info')



if __name__ == '__main__':
    print("Starting the job!")
    app.run(host="0.0.0.0", port=5000)
