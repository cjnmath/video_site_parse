from utiles import get_root
import re
import json
import concurrent.futures



def extract_playlist_url(page_url):
    result = []
    root = get_root(page_url)
    scripts = root.xpath('//script/text()')
    for script in scripts:
        script = script.strip()
        if script.startswith('window.playerAnthology'):
            j_text = re.search('\{.*\}', script).group()
            l = json.loads(j_text)
            break
    for i in l['list']:
        d = {}
        d['title'] = i['title']
        d['url'] = 'https://v.youku.com/v_show/id_{}.html'.format(i['encodevid'])
        d['duration'] = None
        result.append(d)
    return result


def get_duration(page_url):
    root = get_root(page_url)
    scripts = root.xpath('//script/text()')
    for script in scripts:
        script = script.strip()
        if 'window.PageConfig' in script:
            duration = re.search('seconds.+?(\d+)', script)
            if duration:
                return duration.group(1)
    return None


def assemble_data(page_url):
    result = extract_playlist_url(page_url)

    def wrapper(i):
        i['duration'] = get_duration(i['url'])
        print('get youku duration for:', i['url'])

    with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:
        for i in result:
            executor.submit(wrapper, i)
    return result
