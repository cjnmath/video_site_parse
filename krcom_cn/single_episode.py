import requests
from lxml.html import fromstring
import re
import json
import urllib


def get_root(url):
    headers = {}
    headers['User-Agent'] = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.9 Safari/537.36'
    response = requests.get(url, headers=headers)
    response.encoding = 'utf-8'
    return fromstring(response.text)


def parse(target_url):
    headers = {}
    headers['User-Agent'] = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.9 Safari/537.36'
    response = requests.get(target_url, headers=headers)
    response.encoding = 'utf-8'
    title = re.search('CONFIG\[\'title_value\'\]\=\'(.*?)\'\;', response.text).group(1)
    title = re.sub(' 酷燃视频', '', title)
    root = fromstring(response.text)
    r_scripts = root.xpath('//script/text()')
    scripts = [str(i) for i in r_scripts]
    for script in scripts:
        if script.startswith("FM.view"):
            r_d = re.search('FM\.view\((.*)\)', script).group(1)
            d = json.loads(r_d)
            if d.get('domid') == "pl_show_playbox":
                duration = re.search('duration\=(\d+)', d['html']).group(1)
                return title, target_url, duration
