from . import single_episode
import re
import json
from lxml.html import fromstring
import urllib
import concurrent.futures


def get_target_urls(target_url):
    target_urls = []
    root = single_episode.get_root(target_url)
    r_scripts = root.xpath('//script/text()')
    scripts = [str(i) for i in r_scripts]
    for script in scripts:
        if script.startswith('FM.view'):
            r_d = re.search('FM\.view\((.*)\)', script).group(1)
            d = json.loads(r_d)
            if d.get('domid') == "pl_show_season":
                sub_root = fromstring(d['html'])
                r = sub_root.xpath('//div[@node-type="krv_season_box"]//li/a/@href')
    for i in r:
        target_urls.append(urllib.parse.urljoin(target_url, i))
    return target_urls


def parse(target_url):
    target_urls = get_target_urls(target_url)
    results = {}

    def save_to_result(num, url):
        print("Crawling:" + url)
        results[num] = single_episode.parse(url)

    # TODO: better asynchorous mechanism needed
    with concurrent.futures.ThreadPoolExecutor(max_workers=30) as executor:
        n = 1
        for url in target_urls:
            executor.submit(save_to_result, n, url)
            n += 1
    return results
