from utiles import get_root
import json


def extract_all_page_urls(page_url):
    result = []
    root = get_root(page_url)

    r1 = root.xpath('//div[@is="i71-playpage-sdrama-list"]/@*[name()=":initialized-data"]')
    r2 = root.xpath('//div[@is="i71-playpage-source-list"]/@*[name()=":initialized-data"]')
    if r1:
        print('get r1')
        r = r1[0]
    elif r2:
        print('get r2')
        r = r2[0]
    else:
        return result

    l = json.loads(r)
    for i in l:
        contentType = i['contentType']
        if contentType == 1 or contentType == '1':
            r = {}
            r['title'] = i['name']
            r['duration'] = i['duration']
            r['url'] = i['url']
            result.append(r)
    return result
