import requests
import re
import json
from lxml.html import fromstring
import urllib


HEADERS = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/71.0.3578.98 Chrome/71.0.3578.98 Safari/537.36'
}


def extrat_pages_from_av_links(url):
    response = requests.get(url, headers=HEADERS)
    response.encoding = 'utf-8'
    root = fromstring(response.text)
    scripts = root.xpath('//script/text()')
    for script in scripts:
        script = str(script)
        if script.startswith('window.__INITIAL_STATE__='):
            r_d = re.search('(?<=window\.__INITIAL_STATE__=)(\{.+\})\;', script).group(1)
            d = json.loads(r_d)
            break
    if not d:
        return None
    result = {}
    result['parse_result'] = []
    for i in d['videoData']['pages']:

        p_num = str(i['page'])
        p_title = i['part']
        p_duration = i['duration']

        sub_d={}
        sub_d['url'] = urllib.parse.urljoin(url, '?p='+p_num)
        sub_d['title'] = p_title
        sub_d['duration'] = p_duration
        result['parse_result'].append(sub_d)

    return result


# https://www.bilibili.com/bangumi/play/xxxxxx
def extract_episodes_from_bangumi_play_links(url):
    response = requests.get(url, headers=HEADERS)
    response.encoding = 'utf-8'
    root = fromstring(response.text)
    scripts = root.xpath('//script/text()')
    d = None
    for script in scripts:
        script = str(script)
        if script.startswith('window.__INITIAL_STATE__='):
            r_d = re.search('(?<=window\.__INITIAL_STATE__=)(\{.+\})\;', script).group(1)
            d = json.loads(r_d)
            break
    if not d:
        return None

    aids = []
    for i in d['epList']:
        aids.append(i['aid'])
    aids = set(aids)

    def all_positive(l):
        for i in l:
            if i <= 0:
                return False
        return True

    # get durations info from aid(it works for play/epxxxx)
    if all_positive(aids):
        duration_info_list = []
        for aid in aids:
            r = requests.get('https://api.bilibili.com/x/web-interface/view?aid={}'.format(aid), headers=HEADERS)
            duration_info_list.extend(json.loads(r.text)['data']['pages'])
    # for non  play/epxxxx (eg. play/ss24004)
    else:
        duration_info_list = []
        for i in range(len(d['epList'])):
            k = {}
            k['duration'] = d['epList'][i]['aid']
            duration_info_list.append(k)

    # assemble results
    result = {}
    result['parse_result'] = []

    for i in range(len(d['epList'])):
        sub_d = {}
        sub_d['title'] = d['mediaInfo']['title'] + ' - ' + d['epList'][i]['titleFormat'] + ': ' + d['epList'][i]['longTitle']
        # sub_d['url'] = re.sub('\d+\/?$', str(d['epList'][i]['id']), url)
        sub_d['url'] = 'https://www.bilibili.com/bangumi/play/ep' + str(d['epList'][i]['id'])
        sub_d['duration'] = duration_info_list[i]['duration']
        result['parse_result'].append(sub_d)
    return result


def switch_to_play_url_from_media_url(url):
    response = requests.get(url, headers=HEADERS)
    r_ep_id = re.search('\"ep_id\"\:(\d+)', response.text)
    if r_ep_id:
        return "https://www.bilibili.com/bangumi/play/ep" + r_ep_id.group(1)


def extract_episodes_from_bangumi_media_links(url):
    play_url = switch_to_play_url_from_media_url(url)
    if play_url:
        return extract_episodes_from_bangumi_play_links(play_url)
