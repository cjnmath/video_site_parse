import utiles
import urllib


def get_main_frame_roots(root):
    result = []
    r_frames = root.xpath('//div[@class="mdl-cell mdl-cell--12-col mdl-card mdl-shadow--2dp"]')
    if not r_frames:
        return None
    for r_frame in r_frames:
        texts = r_frame.xpath('.//text()')
        if "国语" in ''.join(texts) or "粤语" in ''.join(texts):
            result.append(r_frame)
    return result


def extract_language_type(main_frame_root):
    return main_frame_root.xpath('.//h2/text()')[0]


def extract_episode_names_and_links(main_frame_root, main_url):
    names = main_frame_root.xpath('.//a[@class="mdl-button mdl-button--primary mdl-js-button mdl-js-ripple-effect"]/text()')
    r_links = main_frame_root.xpath('.//a[@class="mdl-button mdl-button--primary mdl-js-button mdl-js-ripple-effect"]/@href')
    links = [urllib.parse.urljoin(main_url, url) for url in r_links]
    return [[a,b] for a, b in zip(names, links)]


def main(url):
    root = utiles.get_root(url)
    main_frame_roots = get_main_frame_roots(root)
    result = {}
    for root in main_frame_roots:
        language_type = extract_language_type(root)
        l = extract_episode_names_and_links(root, url)
        result[language_type] = l

    return result
