from utiles import get_response
import json
import re


HEADERS_1 = {
    'Accept-Encoding': 'identity',
    'Cache-Control': 'no-cache',
    'Host': 'api.cntv.cn',
    'Connection': 'Keep-Alive',
    'User-Agent': 'okhttp/3.9.0'
}


def get_play_src(guid):
    # guid = info['guid']
    url = 'http://vdn.apps.cntv.cn/api/getIpadVideoInfo.do'
    params = {
        'pid': guid,
        'tai': 'ipad',
        'from': 'html'
    }
    response = get_response(url, params=params, headers=HEADERS_1)
    # return response
    t = re.search('html5VideoData = \'(.+)\';getHtml5VideoData', response.text)
    if t:
        d = json.loads(t.group(1))
        # return d['hls_url']
        return d['video']


# 爱看
def get_aikan_info_list(page_num=1):
    result = []
    url = 'http://api.cntv.cn/list/getCboxFeedRecommend'
    params = {
        'serviceId': 'cbox',
        'n': '10',
        'p': str(page_num),
        'type': '0',
        'utdid': 'XJg6b1DaXuIDAJtIdyABt%2FLs'
    }
    response = get_response(url, params=params, headers=HEADERS_1)
    d = json.loads(response.text)
    infos = d['data']['hotRes']
    for info in infos:
        r = {}
        r['page_url'] = 'http://cbox.cntv.cn/special/cbox/fastvideo/index.html?guid=' + info['guid']
        r['image_url'] = info['image']
        r['title'] = info['title']
        r['duration'] = info['length']
        r['tags'] = info['keyword']
        result.append(r)
    # import pprint
    # for info in infos:
    #     pprint.pprint(get_play_src(info))
    #     print('---------------------------')

    return result


# 小央视频
def get_xiaoyanshiping_info_list(page_num=1):
    result = []
    url = "http://media.app.cntvwb.cn/vapi/video/vlist.do"
    params = {
        'gid': '182mmeA30926',
        'n': '10',
        'p': page_num
    }
    headers = HEADERS_1.copy()
    headers['Host'] = 'media.app.cntvwb.cn'
    response = get_response(url, params=params, headers=headers)
    d = json.loads(response.text)
    infos = d['data']
    for info in infos:
        r = {}
        r['page_url'] = info['wwwUrl']
        r['image_url'] = info['image1']
        r['title'] = info['title']
        r['duration'] = info['vduration']
        r['tags'] = info['keywords']
        result.append(r)
    return result


# 新闻
def get_xinwen_info_list(page_num=1):
    result = []
    url = "http://api.cntv.cn/list/CboxSpecialList"
    params = {
        'id': 'TDAT1504838902038321',
        'serviceId': 'cbox',
        'n': '10',
        'p': page_num
    }
    headers = HEADERS_1.copy()
    headers['Host'] = 'api.cntv.cn'
    response = get_response(url, params=params, headers=headers)
    d = json.loads(response.text)
    infos = d['data']
    for info in infos:
        r = {}
        r['page_url'] = "http://cbox.cntv.cn/special/cbox/fastvideo/index.html?guid=" + info['vid']
        r['image_url'] = info['imgUrl']
        r['title'] = info['title']
        r['duration'] = info.get('video_length')
        r['tags'] = ''
        result.append(r)
    return result
