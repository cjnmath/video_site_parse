import requests


class Reporter:
    def __init__(self, web_hook=None):
        self.web_hook = web_hook

    def send_msg(self, msg, at_me=False):

        if at_me:
            at_me_info = {
                            "atMobiles": ["13751741153"],
                            "isAtAll": 'false'
                        }
            msg = msg + '\n @13751741153 请尽快处理！'
        else:
            at_me_info = {}

        url = self.web_hook
        data = {
                    "msgtype": "text",
                    "text": {
                        "content": msg
                    },
                    "at": at_me_info
                }
        response = requests.post(url, json=data)
        return response.status_code == requests.Response.ok

    def send_feed_card(self, feeds):
        """
        feeds [{},{}]
        {} := {'title': XXX,
                'messageURL':XXX,
                'picURL': XXX}
        """
        data = {
                'feedCard':{
                    'links':feeds,
                    },
                "msgtype": "feedCard"
                }
        response = requests.post(self.web_hook, json=data)
        return response.status_code == requests.Response.ok


# QimaiReporter = Reporter('https://oapi.dingtalk.com/robot/send?access_token=6c285b76451156d4f8f8b9d7270ad8a7662272fdc1eb815e667f71cd2746b1a3')
QimaiReporter = Reporter('https://oapi.dingtalk.com/robot/send?access_token=b704273d015c0e1d34fe19bb961fb5b2024005dc9a399a3068125d85d65319ba')
ChuangqiReporter = Reporter('https://oapi.dingtalk.com/robot/send?access_token=9535bc9f7646c7aeaee5a4b64859e5c134f584ca9aa3f4ca1b062270de1b8db0')
